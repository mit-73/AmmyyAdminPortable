# AmmyyAdminPortable
#### Zero-Config Remote Desktop Software Ammyy Admin. The easiest way to establish remote desktop connection.
You can easily share a remote desktop or control a server over the Internet with Ammyy Admin. 
No matter where you are, Ammyy Admin makes it safe and easy to quickly access a remote desktop within a few seconds.

## How to use it:
1) Download and unpack the archive on the way \PatchToPortableApps\
2) Create an "AmmyyAdmin" folder in the path \PatchToPortableApps\AmmyyAdminPortable\App\
3) Download Ammyy Admin from the official website, and move \PatchToPortableApps\AmmyyAdminPortable\App\AmmyyAdmin\
